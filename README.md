<h1 align="center">
 What I Use To Pirate
</h1>

<h2 align="center">
This is not a mega thread
</h2> 

`Last update: 07/04/2022`

# Multi-Purpose

1. [1337x](https://1337x.to/)
- Do not download anything from non-trusted uploaders, example, IGGAMES

2. [rarBG](https://rarbg.to/index80.php)
- Nothing to avoid

3. [rutor](http://www.rutor.info/)
- Use a translator

4. [RuTracker](https://rutracker.org/forum/index.php)
- Use a translator

5. [rustorka](http://rustorka.com/forum/index.php)
- Use a translator

## Private Trackers

1. [FileList](https://filelist.io/)
- Hard to get invited in, only opened free sign up in 2014, never again since

2. [TorrentLeech](https://www.torrentleech.org/)
- They open their sign-up quite often with free invite codes, try your luck from time to time

# Games

1. [KaosKrew](https://kaoskrew.org/)
- Repacks for PC

2. [FitGirl](https://fitgirl-repacks.site/)
- Repacks for PC and Emulation

3. [Dodi](https://dodi-repacks.site/)
- Repacks for PC and Emulation

4. [Gnarly](https://www.gnarly-repacks.site/)
- Repacks for PC and Emulation, mainly older stuff

5. [Darck](https://darckrepacks.com/) (down for now)
- Repacks for PC, they are kind of greedy with the donations for hosting the website

6. [Masquerade](https://masquerade.site/)
- Repacks for PC

7. [ScOOter](https://scooter-repacks.site/)
- Repacks for PC

8. [CPG](https://cpgrepacks.site/)
- Repacks for Anime, Japanese games

9. [GLOAD](https://gload.to/)
- Scene release site, offers plenty of free-user friendly hosters

10. [OldGamesDownload](https://oldgamesdownload.com/)
- Many abandonware classics for a lot of systems

11. [GOG-Games](http://gog-games.com/)
- Official GOG releases

12. [Chovka Repacks](https://repack.info/)
- Repacks

13. [ElAmigos](https://elamigos.site/)
- Repacks

14. [Xatab](https://m.byxatab.com/)
- Repacks (RIP to the OG, maintained by other users)

# Emulation

1. [Emulation General Wiki](https://emulation.gametechwiki.com/index.php/Main_Page)

2. [ROMS Megathread](https://r-roms.github.io/)
- You will find any game you are looking for

3. [DLPS Games](https://dlpsgame.org/home/)
- PS2, PS3, PS4 games

4. [Lakka](http://www.lakka.tv/)
- Lakka is a lightweight Linux distribution that transforms a small computer into a full blown retrogaming console

# Software

1. [nsane.down](https://nsaneforums.com/nsane.down/)
- Software as it should be

2. [DIAKOV](https://diakov.net/)
- Repacks, use a translator

3. [m0nkrus](https://w14.monkrus.ws/)
- Repacks, has a built in english translation

4. [PooShock](http://pooshock.ru/)
- Repacks, use a translator
## Windows
1. [ggOS](https://discord.gg/ggos)
- Gaming and latency focused modification of Windows 10 20h2

2. [AtlasOS](https://atlasos.net/)
- Same as ggOS

3. [Revision](https://sites.google.com/view/meetrevision/revios)
- Less stripped, based on Windows 11, way more general user friendly

# Music

1. [Free MP3 Download](https://free-mp3-download.net/)
- MP3 and FLAC, search engine that allows users to download all their favourite songs, serves the downloads through third party servers

2. [Wacup](https://getwacup.com/)
- WinAmp community update project, aims to make WinAmp still useable while adding many new features and looks, my favourite media player

3. [NightRide FM](https://nightride.fm/stations?station=nightride)
- Synthwave and more radio

4. [BadRadio](https://badradio.nz/)
- Phonk Radio

5. [Core Radio](https://coreradio.ru/)
- The world's first radio station for fans of such music genres as: deathcore, metalcore, post-hardcore, hardcore, and others

# Anime

1. [Anime Piracy Index](https://piracy.moe/)
- This has everything you want for anime

# Sports Streaming

1. [MMA StreamZ](https://redditz.mmastreamlinks.com/)
- MMA Streams

2. [Soccer StreamZ](https://redditz.soccerstreamlinks.com/)
- Football Streams

3. [NBA StreamZ](https://reddits.nbastreamlinks.net/)
- NBA Streams

4. [Boxing StreamZ](https://reddits.boxingstreamlinks.com/)
- Boxing Streams

# Tools for piracy

1. [qBittorrent Enhanced](https://github.com/c0re100/qBittorrent-Enhanced-Edition)
- Unofficial qBittorrent client with modifications

2. [Tixati](https://www.tixati.com/)
- Cross-Platform P2P client

3. [Transmission](https://transmissionbt.com/)
- Lightweight bittorrent client, cross-platform

4. [Deluge](https://deluge-torrent.org/)
- Lightweight, Free Software, cross-platform BitTorrent client

# DO NOT USE

1. **Not recommended sites for pirating games**
- Any website labeled in a way which includes names of scene groups such as CODEX, CPY, RELOADED, SKIDROW and / or others.
- agfy.co
- alltorrents.co
- Cracked-games.org
- crackingpatching.com
- freetp.org
- gamesforyou.co
- getgamez.net
- IGG-Games.com
- loadgames.org
- nosteamgames.ro
- oceanofgames.com
- pcgamestorrents.com
- rgmechanics.com
- steamcracked.co
- steamhub.co
- thepiratebay
- much more because the piracy realm is filled with scammers

2. **uTorrent**
- Has [bundled malware (a bitcoin miner) into the software in the past. The company developing this software is not trustworthy.](https://www.trustedreviews.com/news/utorrent-silently-installing-bundled-bitcoin-mining-software-2931825) Bittorrent (the torrent client) is owned by the same company, not to be confused with qBittorrent.		